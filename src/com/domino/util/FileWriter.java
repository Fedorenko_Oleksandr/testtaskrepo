package com.domino.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class FileWriter {

		public static Boolean wrightResultToFile(String historyNotification){
			try {
				Files.write(Paths.get(FileReader.getFileReaderInstance().getPath("History")), historyNotification.getBytes(), StandardOpenOption.APPEND);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return true;	
				
		}
}
