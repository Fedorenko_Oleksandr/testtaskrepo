package com.domino.util;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.domino.model.Domino;
import com.domino.model.History;

/**Class for work with file
 * 
 * @author Fedorenko Aleksandr
 * @version 1.0
 */
public class FileReader {
	private static final String CONFIG_PATH = System.getProperty("user.dir") + "\\DominoConfig\\";
	private static final String PROJECT_PATH = System.getProperty("user.dir") + "\\";
	private static Logger log = Logger.getLogger(History.class.getName());
	private static FileReader instance;
	
	private FileReader(){
		
	}
	
	public static FileReader getFileReaderInstance(){
		if (instance == null)
            instance = new FileReader();
        return instance;
	}

	public String getPath(String idOfPath){
		List<String> listOfPath = (List<String>) getFileContent(CONFIG_PATH);
		for(String path : listOfPath){
			String[] arrayOfPath = path.split("@");
			if(arrayOfPath[0].equals(idOfPath)){
				return PROJECT_PATH + arrayOfPath[1];
			}
		}
		return null;
	}
	
	public Collection<String> getFileContent(String path){
		List<String> lines;
		try {
			lines = Files.readAllLines(Paths.get(path), StandardCharsets.UTF_8);
			log.info("File:" + path + " read successfully");
			return lines;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Collection<Domino> getDominoesSetFromFile(String path){
		List<String> fileContent = (List<String>) this.getFileContent(path);
		System.out.println(fileContent.get(0));
		 Pattern pattern = Pattern.compile("^[0-6]{1}[:][0-6]{1}$");  
	     Matcher matcher = pattern.matcher(fileContent.get(0));
	     ArrayList<Domino> dominoes = new ArrayList<Domino>();
	     if(matcher.matches()){
	    	 for(String line: fileContent){
	 			Domino domino = new Domino();
	 			domino.setDominoFields(line);
	 			if(!dominoes.contains(domino) && !dominoes.contains(domino.turnDomino())){
					dominoes.add(domino.turnDomino());	
				}
	    	 }
	     }
	     return dominoes;
	}

	public String getView(String view){
		List<String> fileContent = (List<String>) this.getFileContent(getPath(view));
		String content = "";
		for(String line: fileContent){
			content = content + line;
		}
		return content;
	}
    
    public String[] getComboBoxContent(){
    	List<String> comboBoxContent = (List<String>) this.getFileContent(getPath("ComboBoxContent"));
		String type = comboBoxContent.get(0);
		String[] content = {"err"};
		if(type.equals("int")){
			int min = Integer.parseInt(comboBoxContent.get(1));
			int max = Integer.parseInt(comboBoxContent.get(2));
			if(min >= 0 && min <= max ){
				content = new String[max - min + 1];
				for(int i = 0; i < content.length; i++){
					content[i] = Integer.toString(i + min);
				} 
			}
			
		}
		return content;
    }	
}
