package com.domino.view;

import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.JComponent;

import com.domino.util.FileReader;

public class ComboBox extends JComboBox<String> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public JComponent getComboBox(ActionListener comboboxListener){
		JComboBox<Object> comboBox = new JComboBox<Object>(FileReader.getFileReaderInstance().getComboBoxContent());
		comboBox.addActionListener(comboboxListener);
		return comboBox;
	}
}
