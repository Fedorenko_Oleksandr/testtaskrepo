package com.domino.view;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.WindowConstants;

public class MainWindowView extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String APPLICATION_NAME = "Domino";
	
	public JFrame getMainWindow(){
		JFrame mainWindow = new JFrame(APPLICATION_NAME);
		mainWindow.setMinimumSize(new Dimension(380, 380));
		mainWindow.setLocationRelativeTo(null);
		ImageIcon icon = new ImageIcon("Dominoes-icon.png"); 
		mainWindow.setIconImage(icon.getImage());
		mainWindow.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		mainWindow.setLayout(new GridLayout(3, 1));
		return mainWindow;	
	}

}
