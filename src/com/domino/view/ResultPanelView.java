package com.domino.view;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class ResultPanelView {
	public JPanel showResult(String dominoSet, String resultSet, String unusedSet){
		JPanel resultPanel = new JPanel();
		resultPanel.setLayout(new GridLayout(3, 1));
		JLabel inputSetofDominoLable = new JLabel("Set of dominos is " + dominoSet);
		resultPanel.add(inputSetofDominoLable);
		JLabel resultLable = new JLabel("The longest chaine is " + resultSet);
		resultPanel.add(resultLable);
		JLabel unusedDominoLable = new JLabel("Unused dominos: " + unusedSet);
		resultPanel.add(unusedDominoLable);
		return resultPanel;	
	}
}
