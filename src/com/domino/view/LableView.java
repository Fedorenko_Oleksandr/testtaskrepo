package com.domino.view;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.border.Border;

public class LableView extends JLabel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public JComponent getLable(String text, String title){
		JLabel label = new JLabel(text);
		if(title != null){
			Border border = BorderFactory.createTitledBorder(title);
			label.setBorder(border);
		}
		return label;
		
	}

}
