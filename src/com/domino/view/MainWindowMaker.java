package com.domino.view;

import javax.swing.JFrame;
import com.domino.util.FileReader;


public class MainWindowMaker {
	
	public void makeWindow(){
		JFrame mainWindow = new MainWindowView().getMainWindow();
		mainWindow.add(new LableView().getLable(FileReader.getFileReaderInstance().getView("ProblemDefinition.html"), "Definition"));
		
		mainWindow.add(new InputDataView().getDataInputPanel());
		mainWindow.setVisible(true);
	}
	
}



