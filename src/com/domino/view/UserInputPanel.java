package com.domino.view;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class UserInputPanel {
	private static final String INPUT_TYPE = "Input type: ";
	private static final String FILE_PATH = "File: ";
	private static final String NUMBER_OF_DOMINO = "Number of domino: ";
	private static final String DOMINO_SET = "Domino set: ";
	
	public JComponent getUserInputPanel(String typeOfInput, String choosenFile, String numberOfDomino, String dominoSet){
		JPanel userInputPanel = new JPanel();
		String lableText = "";
		
		if(typeOfInput != null){
			lableText = lableText + UserInputPanel.INPUT_TYPE +  typeOfInput;
		}
		if(choosenFile != null){
			lableText = lableText + UserInputPanel.FILE_PATH +  choosenFile;
		}
		if(numberOfDomino != null){
			lableText = lableText + UserInputPanel. NUMBER_OF_DOMINO +  numberOfDomino;
		}
		if(dominoSet != null){
			lableText = lableText + UserInputPanel.DOMINO_SET +  dominoSet;
		}
		
		JLabel userInputLable = new JLabel(lableText); 
		userInputPanel.add(userInputLable);
		
		return userInputLable;
		
		
	}

}
