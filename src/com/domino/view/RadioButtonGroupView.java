package com.domino.view;

import java.awt.event.ActionListener;
import java.util.Collection;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.Border;

public class RadioButtonGroupView {
	public JComponent getRadioButtonGroup(String title, ActionListener al, Collection<String> titles){
		JPanel radioButtonPanel = new JPanel();
		Border border = BorderFactory.createTitledBorder(title);
		ButtonGroup buttonGroup = new ButtonGroup();
		for(String string : titles){
			JRadioButton radioButton = new JRadioButton();
			radioButton.setText(string);
			if(al != null){
				radioButton.addActionListener(al);
			}
			buttonGroup.add(radioButton);
			radioButtonPanel.add(radioButton);
		}
		radioButtonPanel.setBorder(border);
		return radioButtonPanel;
		
	}

}
