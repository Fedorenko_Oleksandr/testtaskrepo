package com.domino.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPanel;

import com.domino.model.AdjacencyMatrix;
import com.domino.model.DominoSet;
import com.domino.solver.BackTrackingSolution;
import com.domino.util.FileReader;

public class InputDataView {

	private JPanel radioButtonPanel;
	private JPanel numberOfDominoChooserPanel;
	
	public JComponent getDataInputPanel(){
		JPanel dataInputPanel = new JPanel();
	
		ActionListener radioButtonListener = new ActionListener() {	
			@Override
			public void actionPerformed(ActionEvent e) {
				AbstractButton aButton = (AbstractButton) e.getSource();
				if(aButton.getText().equals("User input")){
					radioButtonPanel.setVisible(false);
					numberOfDominoChooserPanel.setVisible(true);
				}
				if(aButton.getText().equals("File input")){
					radioButtonPanel.setVisible(false);
					String path = new FileChooserView().getFilePicker();
					if(path != null){
						FileReader fileReader = FileReader.getFileReaderInstance();
						DominoSet userDominoSet = new DominoSet(fileReader.getDominoesSetFromFile(path));
						if(userDominoSet.getDominoSet().isEmpty()){
							radioButtonPanel.setVisible(true);
							String textForJpane = fileReader.getView("WrongFile");
							new ErrorPanel().getErrorPanel(textForJpane, dataInputPanel);
						} else {
							BackTrackingSolution bts = new BackTrackingSolution();
							DominoSet resultSet = new DominoSet(bts.solve(userDominoSet.getDominoSet()));
							ResultPanelView resultPanel = new ResultPanelView();
							String unusedDomino = new AdjacencyMatrix().getDifference(userDominoSet.getDominoSet(), resultSet.getDominoSet());
							dataInputPanel.add(resultPanel.showResult(userDominoSet.getDominoSetAsString(), resultSet.getDominoSetAsString(), unusedDomino));
							dataInputPanel.validate();
							dataInputPanel.repaint();
						}
						
					} else{
						radioButtonPanel.setVisible(true);
					}
				}
				
			}
		};
		
		radioButtonPanel = new JPanel();
		RadioButtonGroupView radioButtonGroupView = new RadioButtonGroupView();
		FileReader fileReader = FileReader.getFileReaderInstance();
		String path = FileReader.getFileReaderInstance().getPath("CheckBoxes");
		radioButtonPanel.add(radioButtonGroupView.getRadioButtonGroup("Choose type Of input", radioButtonListener, fileReader.getFileContent(path)));
		dataInputPanel.add(radioButtonPanel);
		
		ActionListener comboBoxListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JComboBox<String> box = (JComboBox<String>)e.getSource();
                numberOfDominoChooserPanel.setVisible(false);
                DominoSet rundomdominoSet = new DominoSet();
                rundomdominoSet.getRundomDominoes(Integer.parseInt(box.getSelectedItem().toString()));
                BackTrackingSolution bts = new BackTrackingSolution();
                DominoSet resultSet = new DominoSet(bts.solve(rundomdominoSet.getDominoSet()));
                ResultPanelView resultPanelView = new ResultPanelView();
                String unusedDomino = new AdjacencyMatrix().getDifference(rundomdominoSet.getDominoSet(), resultSet.getDominoSet());
                dataInputPanel.add(resultPanelView.showResult(rundomdominoSet.getDominoSetAsString(), resultSet.getDominoSetAsString(),unusedDomino));
				dataInputPanel.validate();
				dataInputPanel.repaint();
			}
		};
		
		numberOfDominoChooserPanel = new JPanel();
		numberOfDominoChooserPanel.add(new ChooseNumberOfDominoSection().getChooseNumberOfDominoSection(null, "Choose number of domino ", comboBoxListener));
		numberOfDominoChooserPanel.setVisible(false);
		dataInputPanel.add(numberOfDominoChooserPanel);
		
		return dataInputPanel;
	}
	
}
