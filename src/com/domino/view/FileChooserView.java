package com.domino.view;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JLabel;

public class FileChooserView extends JFileChooser{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String getFilePicker(){
		JLabel label = new JLabel();
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
		int ret = fileChooser.showDialog(null, "Open file");
		if (ret == JFileChooser.APPROVE_OPTION) {
		    File file = fileChooser.getSelectedFile();
		    label.setText(file.getAbsolutePath());
		    return file.getAbsolutePath();
		    
		}
		return null;
		
	}
}
