package com.domino.view;

import java.awt.event.ActionListener;

import javax.swing.JComponent;
import javax.swing.JPanel;

public class ChooseNumberOfDominoSection extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public JComponent getChooseNumberOfDominoSection(String title, String text, ActionListener comboboxListener){
		JPanel numberOfDominoPanel = new JPanel();
		numberOfDominoPanel.add(new LableView().getLable(text, title));
		numberOfDominoPanel.add(new ComboBox().getComboBox(comboboxListener));
		return numberOfDominoPanel;
	
	}
	
}
