package com.domino.view;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class ErrorPanel extends JOptionPane{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public JComponent getErrorPanel(String error, JPanel frame){
		JOptionPane errorPane = new JOptionPane();
		JOptionPane.showMessageDialog(frame, error, "Error", JOptionPane.WARNING_MESSAGE);
		return errorPane;
	}
}
