package com.domino.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Logger;

/**Class describe "AdjacencyMatrix" entity 
 * intended to convert collection of dominos to adjacency matrix
 * 
 * @author Fedorenko Aleksandr
 * @version 1.0
 */
public class AdjacencyMatrix {
	private static Logger log = Logger.getLogger(AdjacencyMatrix.class.getName());
	
	/**Convert Collection of Dominoes to adjacency matrix
	 * 
	 * Matrix consists from elements 1,0. 1 represent domino bone which
	 * located at intersection of line = value of domino's left field and
	 * row = value of domino's right field.
	 * Example:
	 * Domino set consist of 3 domino bones: bone1 = 3:2, bone2 = 2:2,
	 * bone3 = 4:1;
	 * matrix will take the form:
	 *   0 1 2 3 4 5 6
	 * 0|0 0 0 0 1 0 0
	 * 1|0 0 0 0 0 0 0
	 * 2|0 0 1 1 0 0 0
	 * 3|0 0 1 0 0 0 0
	 * 4|0 1 0 0 0 0 0
	 * 5|0 0 0 0 0 0 0
	 * 6|0 0 0 0 0 0 0
	 * 
	 * @param  Collection<Domino>
	 * @return Byte[][] Adjacency matrix
	 */
	public Byte[][] getMatrix(Collection<Domino> dominoSet){
		Byte[][] matrix = new Byte[7][7];
		for(int i = 0; i < 7; i++){
			for(int j = 0; j < 7; j++){
				matrix[i][j] = 0;
			}
		}
		String logString = "";
		for(Domino domino : dominoSet){
			matrix[domino.getLeftField()][domino.getRightField()] = 1;
			matrix[domino.getRightField()][domino.getLeftField()] = 1;	
			logString = logString + domino.getDominoFieldsString();
		}
		wrightToLog(matrix, logString);
		return matrix;
	}
	
	private void wrightToLog(Byte[][] matrix, String logString){
		String matrixString = "";
		for(int i = 0; i < 7; i++){
			matrixString = matrixString + "\n";
			for(int j = 0; j < 7; j++){
				matrixString = matrixString + matrix[i][j] + " ";
			}
		}
		log.info("Matrix was succesfuly created: Set of domino:" + logString + matrixString );
	}
	
	/**Return string represent of matrix;
	 * 
	 * Example:
	 * 
	 * input matrix:
	 * 0 0 0 0 1 0 0
	 * 0 0 0 0 0 0 0
	 * 0 0 1 1 0 0 0
	 * 0 0 1 0 0 0 0
	 * 0 1 0 0 0 0 0
	 * 0 0 0 0 0 0 0
	 * 0 0 0 0 0 0 0
	 * 
	 * output String: "3:2, 2:2, 4:1".
	 * 
	 * @param Byte[][]
	 * @return String 
	 */
	public String convertMatrixToString(Byte[][] matrix){
		String result = "";
		for(int i = 0; i < 7; i++){
			for(int j = 0; j < 7; j++){
				if(matrix[i][j] == 1) {
					result = result + i + ":" + j + ", "; 
					matrix[j][i] = 0;
				}
			}
		}
		return result.replaceFirst(", $", "");
	}
	
	/**Take difference between two domino set and return string as result;
	 * 
	 * @param Collection<Domino> 
	 * @param Collection<Domino>
	 * @return String 
	 */
	public String getDifference(Collection<Domino> minuendDominoSet, Collection<Domino> subtractor){
		Byte[][] matrix = new Byte[7][7];
		if(minuendDominoSet.size() - subtractor.size() > 0){
			matrix = getMatrix(minuendDominoSet);
			for(Domino domino : (ArrayList<Domino>)subtractor){
				matrix[domino.getLeftField()][domino.getRightField()] = 0;
				matrix[domino.getRightField()][domino.getLeftField()] = 0;
			}
			String result = convertMatrixToString(matrix);
			return result;
		}
		return "all dominos was used";
	}
	

}
