package com.domino.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;
import java.util.logging.Logger;

/**Class describe "DominoSet" entity and store the set of dominos. 
 * two possible variant of creation with default domino bones or predefine 
 * by user. 
 * @author Aleksandr Fedorenko
 * @version 1.0
 */
public class DominoSet {
	private static Logger log = Logger.getLogger(DominoSet.class.getName());
	private ArrayList<Domino> dominoSet;
	private Boolean isStandartDominoSet;
	
	/**fill dominoSet collection by all possible variants of domino bones*/
	public DominoSet(){
		this.isStandartDominoSet = true;
		dominoSet = new ArrayList<Domino>();
		for(int i = 0; i < 7; i ++){
			for(int j = i; j < 7; j++ ){
				Domino domino = new Domino(i, j);
				dominoSet.add(domino);
			}
		}
		log.info("Create new DominoSet with standart set of dominos");
	}
	
	/**fill collection by predefine by user domino bones
	 * if instance was created via this constructor, method 
	 * getRundomDominoes() will return the dominos bones predefine 
	 * by user
	 */
	public DominoSet(Collection<Domino> dominoSet){
		this.isStandartDominoSet = false;
		this.dominoSet = new ArrayList<Domino>(dominoSet);
		log.info("Create new DominoSet:" + getDominoSetAsString());
	}
	
	public Collection<Domino> getDominoSet() {
		return dominoSet;
	}

	public void setDominoSet(Collection<Domino> dominoSet) {
		this.dominoSet = (ArrayList<Domino>) dominoSet;
	}
	
	/**Remove from collection int quantity of random domino bones
	 *
	 * @param int number of random domino bones
	 */
	public void getRundomDominoes(int numberOfDominoes){
		if(this.isStandartDominoSet){
			Random random = new Random();
			int bound = 28;
			int limit = this.dominoSet.size() - numberOfDominoes;
			for(int i = 0; i < limit; i++){
				dominoSet.remove(random.nextInt(bound));
				bound--;
			}
			this.isStandartDominoSet = false;
		}
		log.info("Random DominoSet:" + getDominoSetAsString());
	}	
	
	/**convert dominoSet to string*/
	public String getDominoSetAsString(){
		String dominoSetAsString = "";
		for(Domino domino : dominoSet){
			dominoSetAsString = dominoSetAsString + domino.getDominoFieldsString() + ", ";
		}
		return dominoSetAsString.replaceFirst(", $", "");
	}
	
}
