package com.domino.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.domino.util.FileReader;
import com.domino.util.FileWriter;

/**Class designed for handling results of computation 
 * 
 * @author Fedorenko Aleksandr
 * @version 1.0
 */
public class History {
	
	/** Checks history file for a match with the incoming data
	 *  If a match is found returns collection of domino with results
	 *  
	 * @param Collection<Domino> 
	 * @return Collection<Domino> 
	 */
	public Collection<Domino> checkHistory(Collection<Domino> userInputDominoSet){
		FileReader fileReader = FileReader.getFileReaderInstance();
		List<String> historyNotifications = (List<String>) fileReader.getFileContent(fileReader.getPath("History"));
			for(String line: historyNotifications){
				String[] historyNotation = line.split("#");
				String userInput = convertToHistoryNotation(userInputDominoSet, null);
				if(historyNotation[0].equals(userInput)){
					return convertHistoryNotationToDominoSet(historyNotation[1]);
				}
			}
		return null;
	}

	/** Wright result of computation to file
	 * 
	 * @param Collection<Domino> 
	 * @param Collection<Domino>
	 * @return Boolean 
	 */
	public Boolean saveResultToFile(Collection<Domino> userInputDominoSet, Collection<Domino> result){
		String historyNotification = convertToHistoryNotation(userInputDominoSet, result);
		if(checkHistory(userInputDominoSet) == null){
			return FileWriter.wrightResultToFile(historyNotification);
		}
		return false;
		
	}
	
	/** Convert history string to domino set 
	 * 
	 * @param String  
	 * @return Collection<Domino> 
	 */
	public Collection<Domino> convertHistoryNotationToDominoSet(String historyResultString){
		String[] resultString = historyResultString.split(",");
		ArrayList<Domino> result = new ArrayList<Domino>();
		for(String string : resultString){
			Domino domino = new Domino();
			domino.setDominoFields(string);
			result.add(domino);
		}
		return result;
	}
	
	/** Take collection of domino and convert it to history string
	 * 
	 * @param Collection<Domino>
	 * @param Collection<Domino>
	 * @return String
	 */
	public String convertToHistoryNotation(Collection<Domino> userInputDominoSet, Collection<Domino> result){
		String history = "";
		for(Domino domino : userInputDominoSet){
			history = history + domino.getDominoFieldsString() + ",";
		}
		history = history.replaceFirst(",$", "");
		if(result != null){
			history = history + "#";
			for(Domino domino : result){
				history = history + domino.getDominoFieldsString() + ",";
			}
			history = history.replaceFirst(",$", "") + "\n";
		}
		return history;
	}
}
