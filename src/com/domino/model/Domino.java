package com.domino.model;

import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**Class describe "Domino" entity and store the dominoe's field values 
 * 
 * @author Fedorenko Aleksandr
 * @version 1.0
 *
 */
public class Domino {
	private static Logger log = Logger.getLogger(Domino.class.getName());
	private int leftField;
	private int rightField;
	
	public Domino(){
		
	}
	
	public Domino(int left, int right){
		this.leftField = left;
		this.rightField = right;
	}
	
	public int getLeftField() {
		return leftField;
	}

	public int getRightField() {
		return rightField;
	}

	public void printDominoFields(){
		System.out.println(this.leftField + ":" + this.rightField);
	}
	/**Return string representation of domino's fields
	 * Example:
	 * Domino fields leftField = 3 and rightField = 4 method will return string "3:4"
	 * 
	 * @return String string representation of domino fields  
	 */
	public String getDominoFieldsString(){
		String dominoFields = "";
		dominoFields = this.leftField + ":" + this.rightField;
		return dominoFields;
	}
	/** Get string representation of domino. Validate if the fields match field >= 0 and fields <= 6. 
	 *  Convert string to integer representation which assigns to fields of class.
	 *       
	 * Example:
	 * get string "3:6" convert to this.leftField = 3 and this.rightFields = 6
	 *   
	 * @param String a string representation of dominoe's fields. Valid string consist of  
	 *               two integers between 0 and 6 inclusive separated by ":". 
	 *               Valid string "3:4", invalid strings "0:7", ": ", "inv str".
	 * @return Boolean If string is valid and we were able to assign value to a field of 
	 *                 class return value will be true else false 
	 */
	public Boolean setDominoFields(String domino){
		Pattern p = Pattern.compile("^[0-6]{1}[:]{1}[0-6]{1}$");
		Matcher m = p.matcher(domino);
		if(m.matches()){
			String[] fields = domino.split(":");
			this.leftField = Integer.parseInt(fields[0]);
			this.rightField = Integer.parseInt(fields[1]);
			log.info("New Domino was created:" + leftField + ":" + rightField);
			return true;
		}
		return false;
	}
	
	/**Turn the domino bones if fields of bones was 2:3 will change to 3:2*/
	public Domino turnDomino(){
		if(this.leftField == this.rightField){
			return this;
		} else {
			int temp = this.leftField;
			this.leftField = rightField;
			this.rightField = temp;
			return this;
		}
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + leftField;
		result = prime * result + rightField;
		return result;
	}
		
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Domino other = (Domino) obj;
		if (leftField != other.leftField)
			return false;
		if (rightField != other.rightField)
			return false;
		return true;
	}
}
