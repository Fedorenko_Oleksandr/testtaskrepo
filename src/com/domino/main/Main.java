package com.domino.main;

import com.domino.view.MainWindowMaker;

public class Main {
	
	public static void main(String[] args) {
		MainWindowMaker mwm = new MainWindowMaker();
		mwm.makeWindow();
	}
}
