package com.domino.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import com.domino.model.Domino;
import com.domino.model.History;
import com.domino.util.FileReader;

public class HistoryTest {
	private FileReader fileReader = FileReader.getFileReaderInstance();
	
	@Test
	public void checkHistory(){
		ArrayList<Domino> dominoSetFromFile = (ArrayList<Domino>) fileReader.getDominoesSetFromFile(fileReader.getPath("DominoSetTest"));
		assertEquals(dominoSetFromFile.size(), 6);
		History history = new History();
		ArrayList<Domino> resultFromhistory = (ArrayList<Domino>) history.checkHistory(dominoSetFromFile);
		ArrayList<Domino> result = new ArrayList<>();
		result.add(new Domino(0, 6));
		result.add(new Domino(6, 4));
		result.add(new Domino(4, 2));
		assertEquals(result, resultFromhistory);
	}
}
