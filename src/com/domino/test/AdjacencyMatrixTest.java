package com.domino.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import com.domino.model.AdjacencyMatrix;
import com.domino.model.Domino;
import com.domino.model.DominoSet;

public class AdjacencyMatrixTest {
	private AdjacencyMatrix adjacencyMatrix;
	private DominoSet dominoSet;
	Byte[][] matrix;

	@Before
	public void initTestData(){
		dominoSet = new DominoSet();
		dominoSet.getRundomDominoes(5);
		adjacencyMatrix = new AdjacencyMatrix();
		matrix = adjacencyMatrix.getMatrix(dominoSet.getDominoSet());
	}
	
	@Test
	public void getMatrixTest(){
		AdjacencyMatrix adjacencyMatrix = new AdjacencyMatrix();
		matrix = adjacencyMatrix.getMatrix(dominoSet.getDominoSet());
		for(Domino domino : dominoSet.getDominoSet()){
			int rawLineValue = matrix[domino.getLeftField()][domino.getRightField()];
			int revertRawLineValue = matrix[domino.getRightField()][domino.getLeftField()];
			assertEquals(1, rawLineValue);
			assertEquals(1, revertRawLineValue);
		}
	}
	
	@Test
	public void convertMatrixToStringTest(){
		assertEquals(dominoSet.getDominoSetAsString(), adjacencyMatrix.convertMatrixToString(matrix));
	}
	
	@Test
	public void getDifferenceTest(){
		ArrayList<Domino> setOfDomino = new ArrayList<Domino>();
		setOfDomino.add(new Domino(1, 1));
		setOfDomino.add(new Domino(3, 1));
		setOfDomino.add(new Domino(2, 5));
		DominoSet subtractor = new DominoSet(setOfDomino);
		setOfDomino.add(new Domino(2, 3));
		DominoSet minuendDominoSet = new DominoSet(setOfDomino);
		assertEquals("2:3", adjacencyMatrix.getDifference(minuendDominoSet.getDominoSet(), subtractor.getDominoSet()));
		assertEquals(dominoSet.getDominoSetAsString(), adjacencyMatrix.convertMatrixToString(matrix));
	}
	
}
