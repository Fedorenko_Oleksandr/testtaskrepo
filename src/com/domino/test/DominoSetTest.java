package com.domino.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.domino.model.Domino;
import com.domino.model.DominoSet;

public class DominoSetTest {
	private static ArrayList<Domino> dominoSet;
	private static ArrayList<Domino> standartDominoSet;
	private static DominoSet withParam;
	private static DominoSet withoutParam = new DominoSet();
	
	@SuppressWarnings("unchecked")
	@BeforeClass
	public static void initDominoSets(){
		dominoSet = new ArrayList<Domino>();
		dominoSet.add(new Domino(3, 2));
		dominoSet.add(new Domino(1, 2));
		dominoSet.add(new Domino(6, 2));
		
		standartDominoSet = (ArrayList<Domino>) getStandartDominoSet("list");
	}
	
	@Before
	public void initDominoSetsInstances(){
		withParam = new DominoSet(dominoSet);
		withoutParam = new DominoSet();
	}
	
	@Test
	public void checkSetGetDominoSet(){
		
		assertEquals(withParam.getDominoSet(), dominoSet);
		assertEquals(withoutParam.getDominoSet(), standartDominoSet);
		
		int size = dominoSet.size();  
		ArrayList<Domino> newDominoSet = new ArrayList<Domino>(dominoSet);
		newDominoSet.add(new Domino(5, 2));
		withoutParam.setDominoSet(newDominoSet);
		newDominoSet = (ArrayList<Domino>) withoutParam.getDominoSet();
		assertEquals(size + 1, withoutParam.getDominoSet().size());
		assertEquals(new Domino(5, 2), newDominoSet.get(size));
		
	}
		
	@Test
	public void checkDominoInstance(){

		Assert.assertFalse(withParam.getDominoSet().equals(withoutParam.getDominoSet()));
		assertEquals(withoutParam.getDominoSet(), standartDominoSet);
		assertEquals(withoutParam.getDominoSet().size(), 28);
		assertEquals(withParam.getDominoSet(), dominoSet);	
		assertEquals(withParam.getDominoSet().size(), 3);
	}
	
	@Test
	public void randomDominoTest(){
		
		ArrayList<Domino> withParamSet = new ArrayList<Domino>((ArrayList<Domino>) withParam.getDominoSet());
		assertEquals(withParamSet.size(), 3);
		withParam.getRundomDominoes(2);
		assertEquals(withParamSet.size(), 3);
		assertEquals(withParamSet, dominoSet);
		ArrayList<Domino> rundomWithParam = new ArrayList<Domino>((ArrayList<Domino>) withParam.getDominoSet());
		assertEquals(withParamSet, dominoSet);
		assertEquals(withParam.getDominoSet().size(), 3);
		assertEquals(rundomWithParam, dominoSet);
		assertEquals(withParamSet, rundomWithParam);
		
		ArrayList<Domino> emptyConstructor = new ArrayList<Domino>((ArrayList<Domino>) withoutParam.getDominoSet());
		assertEquals(emptyConstructor, standartDominoSet);
		withoutParam.getRundomDominoes(5);
		ArrayList<Domino> randomWithoutParam = new ArrayList<Domino>((ArrayList<Domino>) withoutParam.getDominoSet());
		
		Assert.assertFalse(randomWithoutParam.equals(standartDominoSet));
		assertEquals(randomWithoutParam.size(), 5);	
		Assert.assertFalse(emptyConstructor.equals(randomWithoutParam));
		
		withoutParam.getRundomDominoes(3);
		assertEquals(withoutParam.getDominoSet().size(), 5);
		assertEquals(withoutParam.getDominoSet(), randomWithoutParam);
		Assert.assertFalse(withoutParam.getDominoSet().equals(standartDominoSet));
	}
	
	@Test
	public void convertToStringTest(){
		
		assertEquals(withoutParam.getDominoSetAsString(), getStandartDominoSet("string"));	
	}
	
	private static Object getStandartDominoSet(String type){
		standartDominoSet = new ArrayList<Domino>();
		String standartDominoSetString = "";
		
		for(int i = 0; i < 7; i++){
			for(int j = i; j < 7; j++){
				standartDominoSetString = standartDominoSetString + i + ":" + j + ", ";
				standartDominoSet.add(new Domino(i, j));
			}
		}
		
		if(type.equals("string")){
			return standartDominoSetString.replaceFirst(", $", "");
		}
		if(type.equals("list")){
			return standartDominoSet;
		}
		return null;
		
	}
}
