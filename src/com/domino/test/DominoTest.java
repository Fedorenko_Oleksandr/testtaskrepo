package com.domino.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.domino.model.Domino;

public class DominoTest {
	Domino domino;
		
	@Test
	public void setDominoFieldsTest(){
		domino = new Domino();
		String validDominoString = "2:4";
		String invalidDominoString = "not valid";
		assertEquals(domino.setDominoFields(validDominoString), true);
		assertEquals(domino.setDominoFields(invalidDominoString), false);
		
		domino.setDominoFields(validDominoString);
		assertEquals(domino.getLeftField(), 2);
		assertEquals(domino.getRightField(), 4);
		assertEquals(domino.setDominoFields("not a valid string"), false);
	}
	
	@Test
	public void getDominoFieldsStringTest(){
		domino = new Domino(2, 0);
		String result = domino.getDominoFieldsString();
		assertEquals(result, "2:0");
		
		domino.setDominoFields("5:3");
		assertEquals(domino.getDominoFieldsString(), "5:3");
		
	}
	
	@Test
	public void turnDominoTest(){
		domino = new Domino(2, 0);
		assertEquals(domino.getLeftField(), 2);
		assertEquals(domino.getRightField(), 0);
		
		domino.turnDomino();
		assertEquals(domino.getLeftField(), 0);
		assertEquals(domino.getRightField(), 2);
	}

}
