package com.domino.solver;

import java.util.Collection;

import com.domino.model.Domino;

public interface ISolver {
	public Collection<Domino> solve(Collection<Domino> userChoise);

}
