package com.domino.solver;

import java.util.ArrayList;
import java.util.Collection;
import com.domino.model.AdjacencyMatrix;
import com.domino.model.Domino;
import com.domino.model.History;

public class BackTrackingSolution implements ISolver {
	private ArrayList<Domino> userSelection;
	private ArrayList<Domino> result = new ArrayList<Domino>();
	private ArrayList<Domino> current = new ArrayList<Domino>();
	private Byte[][] matrix;
	
	
	@Override
	public Collection<Domino> solve(Collection<Domino> dominoSet) {
		this.userSelection = (ArrayList<Domino>) dominoSet;
		matrix = new AdjacencyMatrix().getMatrix(dominoSet);

		History history = new History();
		ArrayList<Domino> historyResult = (ArrayList<Domino>) history.checkHistory(dominoSet);
		if(historyResult != null){
			System.out.println("nxngfn");
			return historyResult;
		}
	
		for(int i = 0; i < 7; i++){
			buildPath(i);
		}
		history.saveResultToFile(userSelection, result);
		return result;
	}
	
	private void buildPath(int line){
		int i = line;
		for(int j = 0; j < 7; j++){
			if(matrix[i][j] == 1){
				Domino domino = new Domino(i, j);
				current.add(domino);
				matrix[i][j] = 0;
				matrix[j][i] = 0;
				buildPath(j);
				matrix[i][j] = 1;
				matrix[j][i] = 1;
			}
		}
		if(current.size() > result.size()){
			result.clear();
			result.addAll(current);
		}
		current.clear();
	}
}
